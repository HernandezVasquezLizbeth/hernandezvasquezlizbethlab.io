 function authenticate() {
    return gapi.auth2.getAuthInstance()
        .signIn({scope: "https://www.googleapis.com/auth/tasks https://www.googleapis.com/auth/youtube"})
        .then(function() { console.log("Sign-in successful"); },
              function(err) { console.error("Error signing in", err); });
  }
  function loadClient() {
    return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/tasks/v1/rest")
        .then(function() { console.log("GAPI client loaded for API"); },
              function(err) { console.error("Error loading GAPI client for API", err); });
  }
  // Make sure the client is loaded and sign-in is complete before calling this method.
  function execute() {
    return gapi.client.tasks.tasklists.list({})
        .then(function(response) {
                // Handle the results here (response.result has the parsed body).
                console.log("Response", response);
              },
              function(err) { console.error("Execute error", err); });
  }
  gapi.load("client:auth2", function() {
    gapi.auth2.init({client_id: '401086402299-00ecnuvi8fkhhrtef6c644bv6bf9j8s8.apps.googleusercontent.com'});
  });

 function loadClient2() {
      gapi.client.setApiKey("AIzaSyB9lqufKYy_SzaMarusqClsYcJB7BI_sNk");
    return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/youtube/v3/rest")
        .then(function() { console.log("GAPI client loaded for API"); },
              function(err) { console.error("Error loading GAPI client for API", err); });
  }

//busca en youtube 
var url;
var idV;
var lati;
var longi;
var out = "";
var q;
var mr;
var ar_video;
var markers = [];
  function buscar() {
    q = $('#busqueda').val(); //obtiene el valor en input de lo que desea buscar
    mr = $('#cantidad').val(); //obtiene la cantidad de videos a buscar
    ar_video = new Array(mr);
    return gapi.client.youtube.search.list({
      "part": "snippet",
      q: q,
      maxResults: mr
    })
        .then(function(response) {
                //console.log("Response", response);
                 var results = response.result;
                 
          $("#results").html("");
          $.each(results.items, function(index, item) { 
            idV = item.id.videoId;
          url = "https://www.youtube.com/embed/"+item.id.videoId;          
                $("#results").append("<p> Titulo: " + item.snippet.title + "</p>");
                $("#results").append("<iframe src="+ url + "></iframe");
                for(i = 0; i < results.items.length; i++) {
                   out+= '<p class=>'+results.items[i].snippet.title+'</p>';
                   ar_video[i]=results.items[i].id.videoId;
    }
          });

              },
              function(err) { console.error("Execute error", err); });
  }

  gapi.load("client");

  ////////////////////////////////////////////////////////////////////////obtener ubicacion de videos///////////////////////////////////////////////////////////////////////////////////

var ar_obj= new Array();
function ubica(){
clearMarkers();
var xmlhttp = new XMLHttpRequest();
var url = "https://www.googleapis.com/youtube/v3/videos?part=recordingDetails%2Cplayer&id="+ar_video+"&key=AIzaSyByOiuXHJYNRx1ajzihEvsa0PB6jCV2eGA";

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myArr = JSON.parse(this.responseText);
    ubica2(myArr);
    }
};

xmlhttp.open("GET", url, true);
xmlhttp.send();
}

//var boton1=document.getElementById('ubica');
//var lati=20;
//var longi=-94;

function ubica2(arr) {
deleteMarkers();
clearMarkers();
    var out = "";
    var i;
   
  for(i = 0; i < arr.items.length; i++) {
  if(typeof arr.items[i].recordingDetails=='undefined'){    
    console.log("No hay videos con ubicacion");
      //document.getElementById("mensaje").textContent = "No hay videos con ubicacion";       
}else{
console.log("Videos con ubicacion");
//document.getElementById("mensaje").textContent = "Videos con ubicacion...";
  //out+= arr.items[i].player.embedHtml+'<p>'+arr.items[i].recordingDetails.location.latitude+'</p><br><p>'+arr.items[i].recordingDetails.location.longitude+'</p>';
   initialize( arr.items[i].recordingDetails.location.latitude, arr.items[i].recordingDetails.location.longitude,arr.items[i].player.embedHtml,"img/yt.png");
   
  }
  
  } 
    //document.getElementById("id02").innerHTML = out;   
}     

  // Make sure the client is loaded before calling this method.
  //Agregar Tarea a una Lista de Tareas
   function agregarTarea() {
    var valor = document.getElementById("busqueda").value;
        //document.getElementById("dato").innerHTML=valor;
    return gapi.client.tasks.tasks.insert({
      "tasklist": "MTIzNTkwMzcwNTM4NDA0MzUyMDY6OTU0MzIxMzYzNTI0Nzc3Mjow",
      "resource": {
        "title": valor
      }
    })
        .then(function(response) {
                console.log("Response", response);
              },
              function(err) { //console.error("Execute error", err); 
            });
  }

  function appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      }

//Permite ver mis busquedas que agregue a mi Lista de Tareas
      function listTask() {
        gapi.client.tasks.tasks.list({
            "tasklist": "MTIzNTkwMzcwNTM4NDA0MzUyMDY6OTU0MzIxMzYzNTI0Nzc3Mjow"
        }).then(function(response) {
          appendPre(' ');
          var tasks = response.result.items;
          if (tasks && tasks.length > 0) {
            for (var i = 0; i < 10; i++) {
              var task = tasks[i];
              appendPre(task.title);
            }
          } else {
            appendPre('No task found.');
          }
        });
      }

////////////////////////////////////////////////////////////twitter////////////////////////////////////////////////////////////////

var search = document.getElementById('busqueda');

function buscarTweet(search){
        var parametros = {
                "busquedaValor" : search,
        };
        $.ajax({
                data:  parametros,
                url:   'twitter.php',
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("");
                },
                success:  function (response) {
                        $("#twitter1").html(response);
                        var obj = JSON.parse(response);
                        console.log(obj);
                        twitterF(obj);


                }
        });
}


function twitterF(response){
console.log(response);
for (var i = 0; i <=response.statuses.length; i++) {
  var texto = '<img style="width:50px; height: 50px" src= "'+ response.statuses[i].user.profile_background_image_url + '">' +  response.statuses[i].text;
   if(response.statuses[i].geo !== undefined && response.statuses[i].geo !== null){
    if(response.statuses[i].geo.coordinates){
       console.log(response.statuses[i].geo.coordinates);
       initialize(response.statuses[i].geo.coordinates[0],response.statuses[i].geo.coordinates[1], texto,"img/twitter.png");
      }else{
    console.log("No tiene");
      }
    }else{
    console.log("No tiene");
  }
}
}

function initialize(lat,lng,frame,icon) {
  var latLng = new google.maps.LatLng(lat, lng);
  var originalMapCenter=new google.maps.LatLng(42.3601,-71.0589);
  var map= {
      zoom: 4,
      center: originalMapCenter    
  };
  var map = new google.maps.Map(document.getElementById("map"),map);
  var marker = new google.maps.Marker({
    position: latLng,
    map: map,
    icon: icon
  });

  var InfoWindow =new google.maps.InfoWindow({
    content:frame,
    maxWidth:500

  })
  google.maps.event.addListener(marker, 'click', function(){InfoWindow.open(map,marker);});
  markers.push(marker);
  setMapOnAll(map);
 }
   
//establecer marcadores 
function setMapOnAll(map) {

        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
}

  //borrarmarcadores
   function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
    //limpiar marcadores
    function clearMarkers() {
        setMapOnAll(null);
      }

function tweet(){
        buscarTweet(search.value);
}